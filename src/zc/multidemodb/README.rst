=============================
Multiple demo storage manager
=============================

Create a demo manager by passing in a base storage and a directory for
changes:

    >>> import os, pprint, zc.multidemodb, ZODB
    >>> base = ZODB.DB('data.fs', cache_size=9999)
    >>> demos = zc.multidemodb.Demos(base, 'changes')

This create an empty changes directory:

    >>> os.listdir('changes')
    []

We can fetch a demo database:

    >>> x = demos['x']

which creates a directory for the changes:

    >>> os.listdir('changes')
    ['x']
    >>> path = os.path.join('changes', 'x')
    >>> pprint.pprint([(name, os.path.isdir(os.path.join(path, name)))
    ...  for name in sorted(os.listdir(path))])
    [('before.tid', False),
     ('changes.fs', False),
     ('changes.fs.index', False),
     ('changes.fs.lock', False),
     ('changes.fs.tmp', False)]


The data in before.tid is the transaction id just after the base
storage's last transaction id:

    >>> before_tid = readfile(os.path.join(path, 'before.tid'))
    >>> from ZODB.utils import u64
    >>> u64(before_tid) == u64(base.storage.lastTransaction()) + 1
    True

It's used to configure the before storage:

    >>> x.storage.base.before == before_tid
    True

We can make changes in the demo storage, which aren't visible in the
base storage:

    >>> with x.transaction() as conn:
    ...    conn.root.data = 1

    >>> with base.transaction() as conn:
    ...    print conn.root()
    {}

The demo databases get the same database parameters as the base
database:

    >>> x._cache_size
    9999

We can reset a demo storage by deleting it. It wil be recreated later,
if necessary:

    >>> del demos['x']

    >>> os.listdir('changes')
    []

    >>> with demos['x'].transaction() as conn:
    ...    print conn.root()
    {}

Lets close the demos and recreate with blobs:

    >>> demos.close()

Note that the data isn't removed.

    >>> os.listdir('changes')
    ['x']
    >>> path = os.path.join('changes', 'x')
    >>> pprint.pprint([(name, os.path.isdir(os.path.join(path, name)))
    ...  for name in sorted(os.listdir(path))])
    [('before.tid', False),
     ('changes.fs', False),
     ('changes.fs.index', False),
     ('changes.fs.lock', False),
     ('changes.fs.tmp', False)]


    >>> base = ZODB.DB('data.fs', cache_size=9999, blob_dir='blobs')
    >>> demos = zc.multidemodb.Demos(base, 'changes')

The data is unchanged until we access a database:

    >>> pprint.pprint([(name, os.path.isdir(os.path.join(path, name)))
    ...  for name in sorted(os.listdir(path))])
    [('before.tid', False),
     ('changes.fs', False),
     ('changes.fs.index', False),
     ('changes.fs.lock', False),
     ('changes.fs.tmp', False)]

But when we open the demos database, we get a blobs directory for it:

    >>> x = demos['x']

    >>> pprint.pprint([(name, os.path.isdir(os.path.join(path, name)))
    ...  for name in sorted(os.listdir(path))])
    [('before.tid', False),
     ('changes.blobs', True),
     ('changes.fs', False),
     ('changes.fs.index', False),
     ('changes.fs.lock', False),
     ('changes.fs.tmp', False)]

Limitations
===========

Multi-database configurations aren't supported.
