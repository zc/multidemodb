
import os
import shutil
import threading
import time
import zc.beforestorage
import ZODB.DemoStorage
import ZODB.FileStorage
import ZODB.interfaces
import ZODB.utils

class Demos:

    def __init__(self, basedb, changes):
        if not os.path.exists(changes):
            os.mkdir('changes')

        self.basedb = basedb
        self.changes = os.path.abspath(changes)
        self.lock = threading.Lock()
        self.dbs = {}

    def __getitem__(self, name):
        with self.lock:
            try:
                return self.dbs[name]
            except KeyError:
                path = os.path.join(self.changes, name)
                beforep = os.path.join(path, 'before.tid')
                basedb = self.basedb
                changesp = os.path.join(path, 'changes.fs')

                if os.path.isfile(beforep):
                    before = readfile(beforep)
                else:
                    os.mkdir(path)
                    before = ZODB.utils.p64(
                        ZODB.utils.u64(basedb.storage.lastTransaction()) + 1)
                    with open(beforep, 'w') as f:
                        f.write(before)

                if ZODB.interfaces.IBlobStorage.providedBy(basedb.storage):
                    changes = ZODB.FileStorage.FileStorage(
                        changesp, blob_dir=os.path.join(path, 'changes.blobs'))
                else:
                    changes = ZODB.FileStorage.FileStorage(changesp)

                before = zc.beforestorage.Before(basedb.storage, before)

                demo = ZODB.DemoStorage.DemoStorage(
                    base=before, changes=changes,
                    close_base_on_close=False,
                    close_changes_on_close=True,
                    )

                db = self.dbs[name] = ZODB.DB(
                    demo,
                    pool_size=basedb.pool.size,
                    pool_timeout=basedb.pool.timeout,
                    historical_pool_size=basedb.historical_pool.size,
                    historical_timeout=basedb.historical_pool.timeout,
                    database_name=basedb.database_name,
                    xrefs=basedb.xrefs,
                    large_record_size=basedb.large_record_size,
                    # XXX ick, underware
                    cache_size=basedb._cache_size,
                    cache_size_bytes=basedb._cache_size_bytes,
                    historical_cache_size=basedb._historical_cache_size,
                    historical_cache_size_bytes=
                    basedb._historical_cache_size_bytes,
                    )

                return db

    def __delitem__(self, name):
        with self.lock:
            path = os.path.join(self.changes, name)
            if os.path.exists(path):
                try:
                    shutil.rmtree(path)
                except Exception:
                    logger.exception("Couldn't remove %r", path)
            self.dbs.pop(name, None)

    def close(self):
        for db in self.dbs.values():
            db.close()
        self.basedb.close()

def readfile(path):
    with open(path) as f:
        return f.read()
